module matrix.multi-proof where

open import Data.Nat using (ℕ)
open import Data.Vec using (Vec)
open import Data.Fin using (Fin; zero; suc)

open import my-fin using (elemAt; finElems; toVec)
open import matrix.multi

open import Relation.Binary.PropositionalEquality using
    (_≡_; refl; cong; sym; trans)
open import Data.Vec.Properties as Vec-Prop using (map-cong)

open import my-fin-proof

-- ofEdges and edge are each other inverse

ofEdges-edge : ∀ {n : ℕ} {g : graph n} → ofEdges (edge g) ≡ g
ofEdges-edge {n} {g} =
    (trans (map-cong (λ x → toVec-elemAt) finElems) toVec-elemAt)

edge-ofEdges :
    ∀ {n : ℕ} {edges : Fin n → Fin n → ℕ} {x y : Fin n} →
    edge (ofEdges edges) x y ≡ edges x y
edge-ofEdges {n} {edges} {x} {y} =
    trans (cong (λ v → elemAt v y) (elemAt-toVec x)) (elemAt-toVec y)

-- ofEdges depends only on the behaviour of its argument
ofEdges-cong :
    ∀ {n : ℕ} {edges edges' : Fin n → Fin n → ℕ} →
    (∀ {u v : Fin n} → edges u v ≡ edges' u v) →
    ofEdges edges ≡ ofEdges edges'
ofEdges-cong {n} {edges} {edges'} edges-ext =
    toVec-cong (λ u → toVec-cong λ v → edges-ext)
