module matrix.multi where

open import Data.Nat using (ℕ)
open import Data.Vec using (Vec; map)
open import Data.Fin using (Fin; zero; suc)

open import my-fin

graph : ℕ → Set
graph n = Vec (Vec ℕ n) n

edge : ∀ {n : ℕ} → graph n → Fin n → Fin n → ℕ
edge m u v = elemAt (elemAt m u) v

ofEdges : ∀ {n : ℕ} → (Fin n → Fin n → ℕ) → graph n
ofEdges edges = toVec λ u → toVec (edges u)

-- degree of a vertex
deg : ∀ {n : ℕ} → graph n → Fin n → ℕ
deg g v = sum (edge g v)

-- a subgraph without the lowest vertex
subGraph : ∀ {n : ℕ} → graph (ℕ.suc n) → graph n
subGraph g = ofEdges (λ u v → edge g (suc u) (suc v))
