module my.multi where

open import Data.Nat using (ℕ; _+_)
open import Data.Vec using (Vec; []; _∷_; lookup; sum)
open import Data.Fin using (Fin; zero; suc)

open import my-fin using (const; sucAt; toVec)

data graph : ℕ → Set where
    empty : graph ℕ.zero
    -- n, forward edges, loops, backward edges
    extend :
        ∀ {n : ℕ} →
        (fwd : Vec ℕ n) → (loops : ℕ) → (bwd : Vec ℕ n) →
        graph n →
        graph (ℕ.suc n)

single : ℕ → graph 1
single loops = extend [] loops [] empty

const-edge : ∀ {n : ℕ} → ℕ → ℕ → ℕ → graph n
const-edge {ℕ.zero} fwd loops bwd = empty
const-edge {ℕ.suc n} fwd loops bwd =
    extend (const fwd) loops (const bwd) (const-edge fwd loops bwd)

edge : ∀ {n : ℕ} → graph n → Fin n → Fin n → ℕ
edge (extend fwd loops bwd g) zero zero = loops
edge (extend fwd loops bwd g) zero (suc v) = lookup fwd v
edge (extend fwd loops bwd g) (suc u) zero = lookup bwd u
edge (extend fwd loops bwd g) (suc u) (suc v) = edge g u v

ofEdges : ∀ {n : ℕ} → (Fin n → Fin n → ℕ) → graph n
ofEdges {ℕ.zero} f = empty
ofEdges {ℕ.suc n} f =
    extend
        (toVec (λ v → f zero (suc v)))
        (f zero zero)
        (toVec (λ v → f (suc v) zero))
        (ofEdges subEdges)
    where subEdges : Fin n → Fin n → ℕ
          subEdges u v = f (suc u) (suc v)

addEdge : ∀ {n : ℕ} → graph n → Fin n → Fin n → graph n
addEdge (extend fwd loops bwd g) zero zero = extend fwd (ℕ.suc loops) bwd g
addEdge (extend fwd loops bwd g) zero (suc v) = extend (sucAt fwd v) loops bwd g
addEdge (extend fwd loops bwd g) (suc u) zero = extend fwd loops (sucAt bwd u) g
addEdge (extend fwd loops bwd g) (suc u) (suc v) =
    extend fwd loops bwd (addEdge g u v)

deg-left : ∀ {n : ℕ} → graph n → Fin n → ℕ
deg-left (extend fwd loops bwd g) zero = 0
deg-left (extend fwd loops bwd g) (suc v) =
    lookup fwd v + lookup bwd v + deg-left g v

deg-loops : ∀ {n : ℕ} → graph n → Fin n → ℕ
deg-loops (extend fwd loops bwd g) zero = loops
deg-loops (extend fwd loops bwd g) (suc v) = deg-loops g v

deg-right : ∀ {n : ℕ} → graph n → Fin n → ℕ
deg-right (extend fwd loops bwd g) zero = sum fwd + sum bwd
deg-right (extend fwd loops bwd g) (suc v) = deg-right g v

deg : ∀ {n : ℕ} → graph n → Fin n → ℕ
deg g v = deg-left g v + deg-loops g v + deg-right g v

subGraph : ∀ {n : ℕ} → graph (ℕ.suc n) → graph n
subGraph (extend fwd loops bwd g) = g

sum-edges : ∀ {n : ℕ} → graph n → ℕ
sum-edges {.ℕ.zero} empty = 0
sum-edges {.(ℕ.suc _)} (extend fwd loops bwd g) =
    sum fwd + loops + sum bwd + sum-edges g
