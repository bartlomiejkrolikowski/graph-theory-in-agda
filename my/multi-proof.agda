module my.multi-proof where

open import Data.Nat using (ℕ; _+_; _*_; _≤_; _≥_)
open import Data.Vec as Vec using (Vec; lookup)
open import Data.Fin using (Fin; zero; suc)
open import Data.Product using (_×_; proj₁; proj₂)
open import Function using (_∘_)

open import my-fin using (toVec; sum)
open import my.multi

open import Relation.Binary.PropositionalEquality using
    (_≡_; refl; cong; sym; trans)
open import Data.Vec.Properties as Vec-Prop using (map-∘)
open import Data.Nat.Properties using (+-assoc; +-comm)

open import my-fin-proof

-- ofEdges and edge are each other inverse

ofEdges-edge : ∀ {n : ℕ} {g : graph n} → ofEdges (edge g) ≡ g
ofEdges-edge {.ℕ.zero} {empty} = refl
ofEdges-edge {.(ℕ.suc _)} {extend fwd loops bwd g} =
    trans
        (cong
            (extend (toVec (lookup fwd)) loops (toVec (lookup bwd)))
            ofEdges-edge
        )
        (cong
            (λ f → f g)
            (trans
                (cong (extend (toVec (lookup fwd)) loops) toVec-elemAt)
                (cong (λ v → extend v loops bwd) toVec-elemAt)
            )
        )

edge-ofEdges :
    ∀ {n : ℕ} {edges : Fin n → Fin n → ℕ} {u v : Fin n} →
    edge (ofEdges edges) u v ≡ edges u v
edge-ofEdges {.(ℕ.suc _)} {edges} {zero} {zero} = refl
edge-ofEdges {.(ℕ.suc _)} {edges} {zero} {suc v} = elemAt-toVec v
edge-ofEdges {.(ℕ.suc _)} {edges} {suc u} {zero} = elemAt-toVec u
edge-ofEdges {.(ℕ.suc _)} {edges} {suc u} {suc v} = edge-ofEdges {u = u} {v = v}

-- ofEdges depends only on the behaviour of its argument
ofEdges-cong :
    ∀ {n : ℕ} {edges edges' : Fin n → Fin n → ℕ} →
    (∀ {u v : Fin n} → edges u v ≡ edges' u v) →
    ofEdges edges ≡ ofEdges edges'
ofEdges-cong {ℕ.zero} {edges} {edges'} edges-ext = refl
ofEdges-cong {ℕ.suc n} {edges} {edges'} edges-ext =
    trans
        (cong
            (λ f → f (ofEdges λ u v → edges (suc u) (suc v)))
            (trans
                (cong
                    (λ f → f (toVec (λ v → edges (suc v) zero)))
                    (trans
                        (cong
                            (λ f → f (edges zero zero))
                            (cong extend (toVec-cong λ v → edges-ext))
                        )
                        (cong
                            (extend (toVec (λ v → edges' zero (suc v))))
                            edges-ext
                        )
                    )
                )
                (cong
                    (extend
                        (toVec (λ v → edges' zero (suc v)))
                        (edges' zero zero)
                    )
                    (toVec-cong λ v → edges-ext)
                )
            )
        )
        (cong
            (extend
                (toVec (λ v → edges' zero (suc v)))
                (edges' zero zero) (toVec (λ v → edges' (suc v) zero))
            )
            (ofEdges-cong edges-ext)
        )

-- addEdge adds an edge
edge-addEdge :
    ∀ {n : ℕ} {g : graph n} {u v : Fin n} →
    edge (addEdge g u v) u v ≡ ℕ.suc (edge g u v)
edge-addEdge {.(ℕ.suc _)} {extend fwd loops bwd g} {zero} {zero} = refl
edge-addEdge {.(ℕ.suc _)} {extend fwd loops bwd g} {zero} {suc v} = elemAt-applyAt {v = fwd}
edge-addEdge {.(ℕ.suc _)} {extend fwd loops bwd g} {suc u} {zero} = elemAt-applyAt {v = bwd}
edge-addEdge {.(ℕ.suc _)} {extend fwd loops bwd g} {suc u} {suc v} = edge-addEdge {g = g}

-- predicates

isIrreflexive : ∀ {n : ℕ} → graph n → Set
isIrreflexive {n} g = ∀ (u : Fin n) → edge g u u ≡ ℕ.zero

isSymmetric : ∀ {n : ℕ} → graph n → Set
isSymmetric {n} g = ∀ (u v : Fin n) → edge g u v ≡ edge g v u

noMultiEdge : ∀ {n : ℕ} → graph n → Set
noMultiEdge {n} g = ∀ (u v : Fin n) → edge g u v ≤ 1

isSimple : ∀ {n : ℕ} → graph n → Set
isSimple g = isIrreflexive g × isSymmetric g × noMultiEdge g

-- sum of degrees is 2 × sum of edges
euler-sum-edges : ∀ {n : ℕ} {g : graph n} → 2 * sum-edges g ≡ sum (deg g)
euler-sum-edges {.ℕ.zero} {empty} = refl
euler-sum-edges {.(ℕ.suc _)} {extend fwd loops bwd g} =
    trans
        (trans
            (trans
                (+-assoc
                    (Vec.sum fwd + loops + Vec.sum bwd)
                    (sum-edges g)
                    (1 * (sum-edges (extend fwd loops bwd g)))
                )
                (cong (λ x → Vec.sum fwd + loops + Vec.sum bwd + x) {!   !})
            )
            (trans
                (cong
                    (λ x → x + Vec.sum bwd + sum (deg (extend fwd loops bwd g) ∘ suc))
                    (+-comm (Vec.sum fwd) loops)
                )
                (cong
                    (λ x → x + sum (deg (extend fwd loops bwd g) ∘ suc))
                    (+-assoc loops (Vec.sum fwd) (Vec.sum bwd))
                )
            )
        )
        (cong
            (λ x → loops + (Vec.sum fwd + Vec.sum bwd) + x)
            (cong
                (Vec.foldr (λ v → ℕ) _+_ 0)
                (map-∘ (λ v → deg (extend fwd loops bwd g) v) suc my-fin.finElems)
            )
        )
-- TODO

-- experimenting:

open import Agda.Builtin.Sigma using (Σ; _,_)

data irrGraph : ℕ → Set where
    irrEmpty : irrGraph ℕ.zero
    -- n, forward edges, backward edges
    irrExtend :
        ∀ {n : ℕ} →
        (fwd : Vec ℕ n) → (bwd : Vec ℕ n) →
        irrGraph n →
        irrGraph (ℕ.suc n)

irreflexiveEmpty : isIrreflexive empty
irreflexiveEmpty ()

irreflexiveExtend :
    ∀ {n : ℕ} (g : graph n) → isIrreflexive g →
    ∀ {fwd : Vec ℕ n} {bwd : Vec ℕ n} →
    isIrreflexive (extend fwd ℕ.zero bwd g)
irreflexiveExtend g P zero = refl
irreflexiveExtend g P (suc u) = P u

irrToMulti : ∀ {n : ℕ} → irrGraph n → Σ (graph n) isIrreflexive
irrToMulti irrEmpty = empty , irreflexiveEmpty
irrToMulti (irrExtend fwd bwd g) = let
    (x , y) = irrToMulti g
    in extend fwd ℕ.zero bwd x , irreflexiveExtend x y

multiToIrr : ∀ {n : ℕ} → Σ (graph n) isIrreflexive → irrGraph n
multiToIrr {.ℕ.zero} (empty , snd) = irrEmpty
multiToIrr {.(ℕ.suc _)} (extend fwd loops bwd fst , snd) =
    irrExtend fwd bwd (multiToIrr (fst , λ u → snd (suc u)))

-- mayBeConnected:
mayBeConnected : ∀ {n : ℕ} → graph n → Set
mayBeConnected {n} g = ∀ (u : Fin n) → Σ (Fin n) (λ v → edge g u v ≥ 0)
