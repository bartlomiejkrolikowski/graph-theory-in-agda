module my.simple where

open import Data.Bool using (Bool; false; true; not)
open import Data.Nat using (ℕ; _+_)
open import Data.Fin using (Fin; zero; suc)
open import Data.Vec using (Vec; []; _∷_; lookup; map; sum)

open import my-bool using (toNat; ofNat)
open import my-fin
    using (const; zero-nequiv-l; zero-nequiv-r; suc-nequiv; applyAt)
    renaming (fin-≢ to _≢_)

open import Relation.Binary.PropositionalEquality using
    (_≡_; refl; cong; sym; trans)
-- open import Relation.Nullary using (¬_)

data graph : ℕ → Set where
    empty : graph 0
    extend :
        ∀ {n : ℕ} →
        (edges : Vec Bool n) →
        graph n →
        graph (ℕ.suc n)

single : graph 1
single = extend [] empty

no-edge : ∀ {n : ℕ} → graph n
no-edge {ℕ.zero} = empty
no-edge {ℕ.suc n} = extend (const false) no-edge

full : ∀ {n : ℕ} → graph n
full {ℕ.zero} = empty
full {ℕ.suc n} = extend (const true) full

edge : ∀ {n : ℕ} → graph n → Fin n → Fin n → ℕ
edge (extend edges g) Fin.zero Fin.zero = 0
edge (extend edges g) Fin.zero (Fin.suc v) = toNat (lookup edges v)
edge (extend edges g) (Fin.suc u) Fin.zero = toNat (lookup edges u)
edge (extend edges g) (Fin.suc u) (Fin.suc v) = edge g u v

const' : ∀ {a b : Set} → a → b → a
const' x y = x

addEdge : ∀ {n : ℕ} → graph n → ∀ (u v : Fin n) → u ≢ v → graph n
addEdge (extend edges g) zero (suc v) (zero-nequiv-l .v) = extend (applyAt (const' true) edges v) g
addEdge (extend edges g) (suc u) zero (zero-nequiv-r .u) = extend (applyAt (const' true) edges u) g
addEdge (extend edges g) (suc u) (suc v) (suc-nequiv .u .v p) = extend edges (addEdge g u v p)

deg-left : ∀ {n : ℕ} → graph n → Fin n → ℕ
deg-left (extend edges g) zero = 0
deg-left (extend edges g) (suc v) =
    toNat (lookup edges v) + deg-left g v

deg-right : ∀ {n : ℕ} → graph n → Fin n → ℕ
deg-right (extend edges g) zero = sum (map toNat edges)
deg-right (extend edges g) (suc v) = deg-right g v

deg : ∀ {n : ℕ} → graph n → Fin n → ℕ
deg g v = deg-left g v + deg-right g v

subGraph : ∀ {n : ℕ} → graph (ℕ.suc n) → graph n
subGraph (extend edges g) = g
