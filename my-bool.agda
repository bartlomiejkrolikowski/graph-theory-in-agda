module my-bool where

open import Data.Bool using (Bool; true; false)
open import Data.Nat using (ℕ; zero; suc; _≤_)
-- open import Agda.Builtin.Sigma using (Σ; _,_)

toNat : Bool → ℕ
toNat false = 0
toNat true = 1

ofNat : ℕ → Bool
ofNat zero = false
ofNat (suc n) = true
