module conversion.conv-proof where

open import Data.Nat using (ℕ)

import matrix.multi
import my.multi

open import conversion.conv

open import Relation.Binary.PropositionalEquality using
    (_≡_; refl; cong; sym; trans)

import matrix.multi-proof
import my.multi-proof

-- matrixToMy and myToMatrix are each other inverse

matrixToMy-myToMatrix :
    ∀ {n : ℕ} {g : my.multi.graph n} → matrixToMy (myToMatrix g) ≡ g
matrixToMy-myToMatrix {n} {g} =
    trans
        (my.multi-proof.ofEdges-cong
            (matrix.multi-proof.edge-ofEdges {edges = my.multi.edge g})
        )
        my.multi-proof.ofEdges-edge

myToMatrix-matrixToMy :
    ∀ {n : ℕ} {g : matrix.multi.graph n} → myToMatrix (matrixToMy g) ≡ g
myToMatrix-matrixToMy {n} {g} =
    trans
        (matrix.multi-proof.ofEdges-cong
            (my.multi-proof.edge-ofEdges {edges = matrix.multi.edge g})
        )
        matrix.multi-proof.ofEdges-edge
