module conversion.conv where

open import Data.Nat using (ℕ)
open import Data.Vec using (Vec; []; _∷_; map)
open import Data.Fin using (Fin; zero; suc)

open import my-fin
open import Function using (_∘_)

import matrix.multi
import my.multi

matrixToMy : ∀ {n : ℕ} → matrix.multi.graph n → my.multi.graph n
matrixToMy = my.multi.ofEdges ∘ matrix.multi.edge

myToMatrix : ∀ {n : ℕ} → my.multi.graph n → matrix.multi.graph n
myToMatrix = matrix.multi.ofEdges ∘ my.multi.edge
