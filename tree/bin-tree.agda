module tree.bin-tree where

open import Data.Nat using (ℕ; _+_)

data BinTree : Set → ℕ → Set where
    leaf : ∀ {a : Set} → BinTree a 1
    node :
        ∀ {a : Set} {n m : ℕ} →
        BinTree a n → BinTree a m →
        BinTree a (ℕ.suc (n + m))
