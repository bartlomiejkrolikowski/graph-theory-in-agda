module tree.tree-conv where

open import Data.Nat using (ℕ; _+_; _≥_)
open import Data.Vec using (Vec; lookup; sum)
open import Data.List using (List; []; _∷_)
open import Data.Fin using (Fin; zero; suc)
open import Agda.Builtin.Sigma using (Σ; _,_)
open import Data.Sum using (_⊎_; inj₁; inj₂)

open import tree.tree using (Tree; size; isBin; isLeaf; isNode)
open import tree.bin-tree using (BinTree; leaf; node)

open import Relation.Binary.PropositionalEquality using
    (_≡_; refl; cong; sym; trans)

BinTree-Tree : ∀ {a : Set} {n : ℕ} → BinTree a n → Tree a
BinTree-Tree BinTree.leaf = Tree.node []
BinTree-Tree (BinTree.node bt bt₁) =
    Tree.node (BinTree-Tree bt ∷ BinTree-Tree bt₁ ∷ [])

Tree-BinTree : ∀ {a : Set} (t : Tree a) → isBin t → BinTree a (size t)
Tree-BinTree (Tree.node []) isLeaf = leaf
Tree-BinTree (Tree.node (t₀ ∷ t₁ ∷ [])) (isNode p₀ p₁) =
    node (Tree-BinTree t₁ p₁) (Tree-BinTree t₀ p₀)
