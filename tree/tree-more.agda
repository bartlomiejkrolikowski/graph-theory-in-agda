module tree.tree-more where

open import Data.Nat using (ℕ; _+_; _≥_)
open import Data.Vec using (Vec; lookup; sum)
open import Data.List using (List; _∷_)
open import Data.Fin using (Fin; zero; suc)
open import Agda.Builtin.Sigma using (Σ; _,_)
open import Data.Sum using (_⊎_; inj₁; inj₂)

open import my.multi using (graph; empty; extend)

open import Relation.Binary.PropositionalEquality using
    (_≡_; refl; cong; sym; trans)

open import my.multi-proof using (mayBeConnected)

NEList : Set → Set
NEList a = Σ a (λ _ → List a)

path : ℕ → Set
path n = Vec (Fin n) n

hasEdge : ∀ {n : ℕ} → graph n → Fin n → Fin n → Set
hasEdge g u v = my.multi.edge g u v ≥ 0

reachableInK : ∀ {n : ℕ} → ℕ → graph n → Fin n → Fin n → Set
reachableInK {n} ℕ.zero g u v = u ≡ v
reachableInK {n} (ℕ.suc k) g u v =
    Σ (Fin n) λ w → hasEdge g u w ⊎ reachableInK k g w v

reachable : ∀ {n : ℕ} → graph n → Fin n → Fin n → Set
reachable g u v = Σ ℕ λ k → reachableInK k g u v

isConnected : ∀ {n : ℕ} → graph n → Set
isConnected {n} g = ∀ (u v : Fin n) → reachable g u v

data SumList : (ℕ → Set) → ℕ → Set where
    nil : ∀ {F : ℕ → Set} → SumList F 0
    cons : ∀ {F : ℕ → Set} {n m : ℕ} → F n → SumList F m → SumList F (n + m)

data BinTree : ℕ → Set where
    leaf : BinTree 0
    node : ∀ {n m : ℕ} → BinTree n → BinTree m → BinTree (ℕ.suc (n + m))

ofGraph : ∀ {n : ℕ} → graph n → BinTree n
ofGraph {.ℕ.zero} empty = leaf
ofGraph {.(ℕ.suc _)} (extend fwd loops bwd g) = node {!   !} {!   !}
-- TODO
