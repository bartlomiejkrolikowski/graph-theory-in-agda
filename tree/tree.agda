module tree.tree where

open import Data.List using (List; []; _∷_;  map; sum)
open import Data.Nat using (ℕ; _+_)

data Tree : Set → Set where
    node : ∀ {a : Set} → List (Tree a) → Tree a

size : ∀ {a : Set} → Tree a → ℕ
size (node []) = 1
size (node (x ∷ x₁)) = size (node x₁) + size x

data isBin : ∀ {a : Set} → Tree a → Set where
    isLeaf : ∀ {a : Set} → isBin {a = a} (node [])
    isNode :
        ∀ {a : Set} {t₀ t₁ : Tree a} →
        isBin t₀ → isBin t₁ →
        isBin (node (t₀ ∷ t₁ ∷ []))
