module my-fin where

open import Data.Nat using (ℕ)
open import Data.Vec as Vec using (Vec; []; _∷_; map; lookup)
open import Data.Fin using (Fin; zero; suc)

const : ∀ {a : Set} {n : ℕ} → a → Vec a n
const {a} {ℕ.zero} x = []
const {a} {ℕ.suc n} x = x ∷ const x

-- a list of all elems of (Fin n)
finElems : ∀ {n : ℕ} → Vec (Fin n) n
finElems {ℕ.zero} = []
finElems {ℕ.suc n} = zero ∷ map suc finElems

toVec : ∀ {a : Set} {n : ℕ} → (Fin n → a) → Vec a n
toVec f = map f finElems

elemAt : ∀ {a : Set} {n : ℕ} → Vec a n → (Fin n → a)
elemAt = lookup

sum : ∀ {n : ℕ} → (Fin n → ℕ) → ℕ
sum f = Vec.sum (toVec f)

applyAt : ∀ {a : Set} {n : ℕ} → (a → a) → Vec a n → Fin n → Vec a n
applyAt f (x ∷ v) zero = f x ∷ v
applyAt f (x ∷ v) (suc i) = x ∷ applyAt f v i

sucAt : ∀ {n : ℕ} → Vec ℕ n → Fin n → Vec ℕ n
sucAt v i = applyAt ℕ.suc v i

data fin-≢ : ∀ {n : ℕ} → Fin n → Fin n → Set where
    zero-nequiv-l : ∀ {n : ℕ} (f₁ : Fin n) → fin-≢ zero (suc f₁)
    zero-nequiv-r : ∀ {n : ℕ} (f₀ : Fin n) → fin-≢ (suc f₀) zero
    suc-nequiv :
        ∀ {n : ℕ} (f₀ f₁ : Fin n) →
        fin-≢ f₀ f₁ →
        fin-≢ (suc f₀) (suc f₁)

-- _≢_ : ∀ {n : ℕ} → Fin n → Fin n → Set
-- _≢_ = fin-≢
