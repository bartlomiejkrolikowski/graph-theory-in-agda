module my-fin-proof where

open import Data.Nat using (ℕ)
open import Data.Vec as Vec using (Vec; []; _∷_; lookup; map)
open import Data.Fin using (Fin; zero; suc)
open import Function using (_∘_)
open import Data.Empty using (⊥; ⊥-elim)

open import my-fin

open import Relation.Binary.PropositionalEquality using
    (_≡_; _≢_; _≗_; refl; cong; sym; trans)
open import Relation.Nullary using (¬_)
open import Data.Vec.Properties as Vec-Prop using (map-∘)

-- auxiliary
toVec-comp :
    ∀ {a : Set} {n : ℕ} {f : Fin (ℕ.suc n) → a} →
    toVec (f ∘ suc) ≡ map f (toVec suc)
toVec-comp {_} {_} {f} = map-∘ f suc finElems

-- toVec and elemAt are each other inverse

toVec-elemAt : ∀ {a : Set} {n : ℕ} {v : Vec a n} → toVec (elemAt v) ≡ v
toVec-elemAt {a} {.ℕ.zero} {[]} = refl
toVec-elemAt {a} {.ℕ.suc _} {x ∷ v} =
    cong (λ v' → x ∷ v') (trans (sym toVec-comp) toVec-elemAt)

elemAt-toVec : ∀ {a : Set} {n : ℕ} {f : Fin n → a} → elemAt (toVec f) ≗ f
elemAt-toVec {a} {.(ℕ.suc _)} {f} zero = refl
elemAt-toVec {a} {.(ℕ.suc _)} {f} (suc x) =
    trans (cong (λ v → lookup v x) (sym toVec-comp)) (elemAt-toVec x)

-- toVec depends only on the behaviour of its argument
toVec-cong : ∀ {a : Set} {n : ℕ} {f g : Fin n → a} → f ≗ g → toVec f ≡ toVec g
toVec-cong {a} {ℕ.zero} {f} {g} p = refl
toVec-cong {a} {ℕ.suc n} {f} {g} p =
    trans (cong (λ x → x ∷ map f (map suc finElems)) (p zero))
        (cong
            (λ v → (g zero) ∷ v)
            (trans (sym toVec-comp) (trans (toVec-cong (p ∘ suc)) toVec-comp))
        )

-- applyAt applies the function at the given position
elemAt-applyAt :
    ∀ {a : Set} {n : ℕ} {f : a → a} {v : Vec a n} {i : Fin n} →
    elemAt (applyAt f v i) i ≡ f (elemAt v i)
elemAt-applyAt {a} {.(ℕ.suc _)} {f} {x ∷ v} {zero} = refl
elemAt-applyAt {a} {.(ℕ.suc _)} {f} {x ∷ v} {suc i} = elemAt-applyAt {v = v}

-- fin-nequiv does not exists for equal arguments
fin-≢-to-⊥ : ∀ {n : ℕ} {f : Fin n} → ¬ fin-≢ f f
fin-≢-to-⊥ {.(ℕ.suc _)} {.(suc f₀)} (suc-nequiv f₀ .f₀ fne) = fin-≢-to-⊥ fne

-- fin-nequiv is equivalent to _≢_
fin-≢-to-≢ : ∀ {n : ℕ} {f₀ f₁ : Fin n} → fin-≢ f₀ f₁ → f₀ ≢ f₁
fin-≢-to-≢ {.(ℕ.suc _)} {.(suc f₀)} {.(suc f₀)} (suc-nequiv f₀ .f₀ fne) refl =
    fin-≢-to-≢ fne refl

≢-to-fin-≢ : ∀ {n : ℕ} {f₀ f₁ : Fin n} → f₀ ≢ f₁ → fin-≢ f₀ f₁
≢-to-fin-≢ {.(ℕ.suc _)} {zero} {zero} p = ⊥-elim (p refl)
≢-to-fin-≢ {.(ℕ.suc _)} {zero} {suc f₁} p = zero-nequiv-l f₁
≢-to-fin-≢ {.(ℕ.suc _)} {suc f₀} {zero} p = zero-nequiv-r f₀
≢-to-fin-≢ {.(ℕ.suc _)} {suc f₀} {suc f₁} p =
    suc-nequiv f₀ f₁ (≢-to-fin-≢ λ x → p (cong suc x))
